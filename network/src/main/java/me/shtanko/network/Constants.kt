package me.shtanko.network

const val GSON_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"
const val GITHUB_API_URL = BuildConfig.GITHUB_API_URL