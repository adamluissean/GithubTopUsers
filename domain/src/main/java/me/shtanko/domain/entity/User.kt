package me.shtanko.domain.entity

data class User(
    val id: Int,
    val login: String,
    val avatarUrl: String
)