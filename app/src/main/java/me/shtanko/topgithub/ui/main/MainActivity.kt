package me.shtanko.topgithub.ui.main

import android.os.Bundle
import me.shtanko.topgithub.R
import me.shtanko.topgithub.platform.BaseActivity

class MainActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.container_activity)
        if (savedInstanceState == null) {
            navigator.openMainFragment(this)
        }
    }
}
